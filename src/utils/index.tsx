import React from "react"
import {Icon, TopNavigationAction} from '@ui-kitten/components';

const useInputChanges = (initialValue = '') => {
    const [value, setValue] = React.useState(initialValue);
    return {
        value,
        onChangeText: setValue,
    };
};

const useRadioChanges = (initialValue = '') => {
    const [value, setValue] = React.useState(initialValue);
    return {
        selectedIndex:value,
        onChange: setValue,
    };
};

const BackIcon = (style) => (
    <Icon {...style} name='arrow-back' fill='#FFF'/>
);

export const BackAction = ({onPress}) => (
    <TopNavigationAction icon={BackIcon} onPress={onPress}/>
);


export const utils  = {
    useInputChanges,
    useRadioChanges
}
