import React from 'react';
import { NavigationNativeContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { LoginScreen } from '../scenes/authen/login.screen';
import { LoadingScreen } from '../scenes/loading.screen';
import { MainScreen } from '../scenes/main.screen';
import { SurveyScreen } from '../scenes/survey/survey.screen';
import { SurveyResultScreen } from '../scenes/survey/result.screen';
import { ArticleMenu } from '../scenes/article/menu.screen';
import { ArticleContent } from '../scenes/article/content.screnn';
import { CondomContent } from '../scenes/article/condom.screnn';
import { RegisterScreen } from '../scenes/authen/register.screen';
import { SubMenu1Screen } from '../scenes/article/sub-menu-1.screen';


const Stack = createStackNavigator();

export const AppNavigator = (): React.ReactElement => (
  <NavigationNativeContainer>
    {/*<HomeNavigator/>*/}
      <Stack.Navigator initialRouteName='login' headerMode='none'>
          <Stack.Screen name='login' component={LoginScreen} options={{ title: 'เข้าสู่ระบบ' }}/>
          <Stack.Screen name='register' component={RegisterScreen} options={{ title: "ลงทะเบียน" }}/>
          <Stack.Screen name='loading' component={LoadingScreen}/>
          <Stack.Screen name='main' component={MainScreen}/>
          <Stack.Screen name='survey' component={SurveyScreen}/>
          <Stack.Screen name='survey-result' component={SurveyResultScreen}/>
          <Stack.Screen name='article-menu' component={ArticleMenu}/>
          <Stack.Screen name='sub-menu-1' component={SubMenu1Screen}/>
          <Stack.Screen name='article-content' component={ArticleContent}/>

          {/*<Stack.Screen name='condom-content' component={CondomContent}/>*/}
      </Stack.Navigator>
  </NavigationNativeContainer>
);
