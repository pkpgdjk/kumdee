import {StyleSheet} from 'react-native';
import {COLORS} from './colors';

export const globalStyle = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: COLORS.BG
    },
    headerTitleText:{
        fontSize:28,
        color:"#FFF",

    },
    headerTitle:{
        backgroundColor:COLORS.HARD_RED_1,

        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation: 5,
        position:'relative',
    }
});
