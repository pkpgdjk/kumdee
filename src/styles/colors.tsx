export const COLORS: any = {
    BG:'#fae3d9',

    SOFT_RED:"#fae3d9",

    TEAL:"#bbded6",

    BLUE:"#8ac6d1",
    BLUE_1:"#bad0d1",
    BLUE_2:"#90cad1",
    BLUE_3:"#67c5d1",

    HARD_RED:"#ff646e",
    HARD_RED_1:"#ff8c85",
    HARD_RED_2:"#ff8082",


    RED:"#ffb6b9",
    RED_1:"#fffbfb",
    RED_2:"#ffdadc",
    RED_3:"#ff9494",

    ARTICLE_TEXT:"#8a8a8a"
};
