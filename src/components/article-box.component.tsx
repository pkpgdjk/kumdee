import {StyleSheet, TouchableOpacity, View,Text as RText} from 'react-native';
import React,{useState,useEffect,useRef} from 'react';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Text} from '@kitten/ui';
import LottieView from "lottie-react-native";
import {COLORS} from '../styles/colors';

const RenterHeader = (props) : React.ReactElement => {
    const plus = useRef()
    const minus = useRef()
    const [expand, setExpand] = useState(false);

    useEffect(()=>{

    },[])

    useEffect(()=>{
        if (props.section.render){
            if (props.isExpand && plus.current){
                plus.current.play(40,60);
            }else{
                plus.current.play(110,130);
            }
        }
    },[props.isExpand])

    let textStyle = [styles.headerText]
    if (props.section.headerSize){
        textStyle.push({fontSize:props.section.headerSize})
    }

    return (
        <View style={[styles.card,styles.header, {justifyContent: props.section.textAlign || "space-between" }]} >
            <Text style={textStyle} category={'h2'}
            >{props.section.header}</Text>
            {props.section.render && <LottieView
                ref={plus}
                loop={false}
                autoPlay={false}
                colorFilters={['#ffb6b9','#ffb6b9','#ffb6b9']}
                style={{
                    width: 40,
                    height: 40,
                }}
                source={require('../assets/lottie/expand-2.json')}
            />}
        </View>
    );
};


const RenterBody = (props): React.ReactElement => {
    if (!props.section.render && !props.section.body){
        return <View></View>
    }
    return (
        <View style={{flex:1,justifyContent:'center',alignItems:'center',paddingVertical:3,marginBottom:20}}>
            <View style={[styles.card,styles.body]}>
                {(()=>{
                    if (props.section.render) {
                        return props.section.render()
                    }
                    else if (props.section.body) {
                        return <Text style={[styles.bodyText]}>{ props.section.body}</Text>
                    }else {
                        return <View></View>
                    }
                })()}
            </View>
        </View>

    );
};



export const _handleRender = (section,type,active) =>{
    return type === 'header' ? <RenterHeader section={section} isExpand={active.includes(section.index)} /> : <RenterBody section={section}  isExpand={active.includes(section.index)}/>;
}


const styles = StyleSheet.create({
    card:{
        backgroundColor:'#FFF',
        paddingVertical:20,
        paddingHorizontal:20,
        width:wp('90'),
        marginVertical:5,
        borderRadius:5,
        display:"flex",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap:'wrap',


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    header:{
        paddingVertical: 7,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    headerText:{
        color:"#ff646e"
    },
    body:{
        width:wp('85%'),
        marginVertical:0,
    },
    bodyText:{
        fontSize:23,
        color:COLORS.ARTICLE_TEXT
    }
});
