import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import {COLORS} from '../styles/colors';
import {borderColor, borderWidth} from '@eva-design/eva/mapping';

export const Button = (props): React.ReactElement => {
    const onPress = () =>{
        if (!props.disabled && props.onPress) {
            props.onPress();
        }
    }

    let propsStyle = {}
    let propsTextStyle = {}

    if (props.color){
        propsStyle['backgroundColor'] = props.color
    }

    if (props.outline){
        propsStyle['backgroundColor'] = undefined
        propsStyle['borderColor'] = props.color || COLORS.HARD_RED
        propsStyle['borderWidth'] = 1
        propsStyle['shadowOpacity'] = 0
        propsStyle['elevation'] = 0

        propsTextStyle['color'] = props.color || COLORS.HARD_RED
    }


    if (props.disabled){
        propsStyle['backgroundColor'] = '#ccc'
        propsStyle['shadowOpacity'] = 0
        propsStyle['elevation'] = 0
    }

    return (
        <TouchableOpacity style={[styles.wrapper,propsStyle,props.style]} onPress={onPress}>
            <Text style={[styles.text,propsTextStyle,props.textStyle]}>{props.children}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    wrapper:{
        borderRadius:5,
        backgroundColor: COLORS.HARD_RED,
        paddingVertical:12,
        paddingHorizontal:50,
        marginVertical:14,

        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation: 7,
        position:'relative',
    },
    text:{
        fontSize:28,
        fontFamily:'opensans-regular',
        color:"#FFF",
        textAlign: 'center',

    }
})
