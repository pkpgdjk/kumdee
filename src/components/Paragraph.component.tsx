import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Text} from '@ui-kitten/components';


export const Paragraph = ({bgColor,width,children,paddingTop}): React.ReactElement => {
    return (
        <View style={{
            backgroundColor: bgColor || '#2dc3ff',
            width: wp(width || '100%'),
            paddingHorizontal: 15,
            paddingVertical: 15,
            paddingTop,
        }}>
            <View style={{
                backgroundColor:'rgba(255,255,255,0.16)',
                borderRadius:5,
                paddingHorizontal: 10,
                paddingVertical: 5,
            }}>
                {children}
            </View>
        </View>
    );
};

export const ParagraphText = ({color,children}): React.ReactElement => {
   return (
       <Text category={'h4'} style={{
           color: color || '#fff',
           // paddingHorizontal: 10,
           // paddingVertical: 5,
           textShadowColor: color || '#FFF',
           textShadowOffset: {width: 0, height: 0},
           textShadowRadius: 5,
       }}> {
           children.replace ? (children.replace(/\\t/g, "\t\t\t\t")
                   .replace(/\\n/g,"\n"))
               : children
       } </Text>
   )
}

export const ParagraphMiniHeader = ({children,textAlign,width,bgColor,color}): React.ReactElement => {
    return (
        <View style={{
            left:-20,
            backgroundColor: bgColor || "#ffd34b",
            borderRadius:5,
            paddingVertical:5,
            paddingHorizontal:15,
            width:width||100,
            marginVertical:10,
        }}>
            <Text category={"h4"} style={{
                textAlign:textAlign||'center',
                color:color || "#FFF",
            }}>{children}</Text>
        </View>

    );


};
