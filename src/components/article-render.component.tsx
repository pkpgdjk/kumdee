import React from 'react';
import {Text} from '@kitten/ui';
import {Image, View} from 'react-native';
import {COLORS} from '../styles/colors';


export const AtText = (props) : React.ReactElement => {
    return (
        <Text style={[{
            color:COLORS.ARTICLE_TEXT,
            fontSize:23,
            marginVertical:props.mv
        },props.style]}>{props.text}</Text>
    )
}

export const AtImg = (props) : React.ReactElement => {
    return (
        <View style={{flex:1,justifyContent:'center',alignItems:'center',marginVertical:15}}>
            <Image style={[{
                width:props.width || 150,
                height:props.height || 150,
                borderRadius:props.br ||5,
                marginVertical:props.mv,
                marginHorizontal:props.mh
            },props.style]}
                   source={props.source}
            />
        </View>
    )
}

export const AtImg2 = (props) : React.ReactElement => {
    return (
        <View style={{flex:1,justifyContent:'center',alignItems:'center',marginVertical:10,flexDirection:'row'}}>
            <Image style={[{
                width:props.width || 125,
                height:props.height || 125,
                borderRadius:props.br ||5,
                marginVertical:props.mv,
                marginHorizontal:props.mh || 5
            },props.style]}
                   source={props.source[0]}
            />

            <Image style={[{
                width:props.width || 125,
                height:props.height || 125,
                borderRadius:props.br ||5,
                marginVertical:props.mv,
                marginHorizontal:props.mh || 5
            },props.style]}
                   source={props.source[1]}
            />
        </View>
    )
}
