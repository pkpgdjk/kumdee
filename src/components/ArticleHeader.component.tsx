import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Text} from '@ui-kitten/components';


export const ArticleHeader = ({bgColor,width,color,children,textAlign}): React.ReactElement => {
    return (
            <Text category={'h1'} style={{
                backgroundColor: bgColor || '#2dc3ff',
                width: wp(width || '100%'),
                paddingHorizontal: 15,
                paddingVertical: 10,
                textAlign:textAlign||"center",
                display: 'flex',
                color: color || '#fff',
                textShadowColor: color || '#FFF',
                textShadowOffset: {width: 0, height: 0},
                textShadowRadius: 5,
            }}> {children.replace('\\t', "\t\t\t\t")} </Text>
    );


};
