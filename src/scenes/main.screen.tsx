import React, {useState} from 'react';
import {SafeAreaLayout} from '../components/safe-area-layout.component';
import {Image, StyleSheet, View} from 'react-native';
import {Input, Text} from '@kitten/ui';
import {Button} from '../components/button-component';
import {COLORS} from '../styles/colors';
import {globalStyle} from '../styles';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {CheckBox, Layout, Modal} from '@ui-kitten/components';
import {AsyncStorage} from 'react-native';
import axios from 'axios';
import {CONSTANTS} from '../constants';
import {LoadingScreen} from './loading.screen';
import { Audio } from 'expo-av';


export const MainScreen = (props): React.ReactElement => {
    const [visible, setVisible] = React.useState(false);
    const [checked, setChecked] = React.useState(false);
    const [profile, setProfile] = React.useState(null);
    const [loading, setLoading] = useState(true);

    const toggleModal = () => {
        setVisible(!visible);
    };
    const init = async () =>{

        try {
            setLoading(true);
            let profile = await axios.get(CONSTANTS.URL + '/profile')
            console.log('GET PROFILE', profile.data.data);
            await AsyncStorage.setItem('profile',JSON.stringify(profile.data.data));
            setProfile(profile.data.data);
            setLoading(false);
            // props.navigation.navigate('main')



            // await AsyncStorage.setItem('notShowModal', "0");
            const value = await AsyncStorage.getItem('notShowModal');
            console.log("GET MODAL",value)
            if (value === null || value !== "1") {
                setVisible(true)
            }
        } catch (error) {
            props.navigation.navigate('login')
            if (error.response){
                console.log(error.response)
            }else {
                console.log(error)
            }
        }
    }
    React.useEffect(()=>{
        const unsubscribe = props.navigation.addListener('focus', () => {
            init()
        });
        return unsubscribe;
    },[])

    const onCloseModal = async ()=>{
        console.log(checked);
        if (checked){
            try {
                await AsyncStorage.setItem('notShowModal', "1");
            } catch (error) {
                console.log(error)
            }
        }
        setVisible(false)
    }
    if (loading){
        return <LoadingScreen/>;
    }
    return (
        <SafeAreaLayout
            style={globalStyle.safeArea}
            insets='top'
        >
            <View style={styles.layout}>
                <Image
                    style={{width: 250, height: 250}}
                    source={require('../assets/img/logo.png')} />

                <View style={styles.card}>
                    <Text category={'h1'} style={styles.header}>เมนูการใช้งาน</Text>

                    <Button style={styles.btnBar} disabled={profile.statusPre} onPress={()=>props.navigation.navigate('survey', {type: 'pre'})}> แบบทดสอบ ก่อนใช้งาน </Button>
                    <Button style={styles.btnBar} onPress={()=>props.navigation.navigate('article-menu',{type:"menu1"})}> วิธีการคุมกำเนิด </Button>
                    <Button style={styles.btnBar} disabled={!profile.statusPre || profile.statusPost} onPress={()=>props.navigation.navigate('survey', {type: 'post'})}> แบบทดสอบ หลังใช้งาน</Button>

                </View>
            </View>

            <Modal
                backdropStyle={styles.backdrop}
                onBackdropPress={toggleModal}
                visible={visible}>
                <Layout
                    level='3'
                    style={styles.modalContainer}>
                    <Text category={"h2"}>วัตถุประสงค์</Text>
                    <Text category={"h4"} style={{marginBottom:15}}>แอปพลิเคชั่นนี้จัดทำขึ้นเพื่อต้องการเผยแพร่ความรู้เกี่ยวกับวิธีการคุมกำเนิดแบบต่างๆให้กับวัยรุ่นได้รู้จักวิธีป้องกันการตั้งครรภ์ที่ไม่พึงประสงค์เพื่อให้ดำเนินชีวิตเป็นไปอย่างมีประสิทธิภาพมากขึ้น</Text>
                    <CheckBox
                        text={"ไม่ต้องแสดงหน้านี้อีก"}
                        checked={checked}
                        textStyle={{fontSize:25}}
                        onChange={setChecked}
                    />
                    <Button style={{marginVertical:25}} onPress={onCloseModal}>ปิด</Button>
                </Layout>
            </Modal>
        </SafeAreaLayout>
    )
}


const styles = StyleSheet.create({
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: wp('90%'),
        paddingVertical: 32,
        paddingHorizontal: 22,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    layout: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    header:{
        // fontSize: 30,
        color: COLORS.HARD_RED,
        // fontWeight:'1000'
    },
    card:{
        backgroundColor:"#FFF",
        paddingVertical:20,
        paddingHorizontal:30,
        width:wp('85'),
        borderRadius:5,
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:3,
        position:'relative',
    },
    btnBar:{
        alignSelf: 'stretch',
    }
});
