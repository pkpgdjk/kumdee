import React, {useEffect, useRef, useState} from 'react';
import preTest from '../survey/pre.json';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {globalStyle} from '../../styles';
import {Text, TopNavigation} from '@kitten/ui';
import {Image, StyleSheet, View, TouchableOpacity} from 'react-native';
import {COLORS} from '../../styles/colors';
import LottieView from "lottie-react-native";
import {Button} from '../../components/button-component';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Icon, TopNavigationAction} from '@ui-kitten/components';
import {BackAction} from '../../utils';

export const ArticleMenu = (props): React.ReactElement => {
    const [type,setType] = React.useState( null)
    useEffect(() => {
        if (props.route && props.route.params && props.route.params.type) {
            setType(props.route.params.type || null);
        }
    }, []);


    const onPress = (menu) =>{
        switch (menu) {
            case 'condom':
                props.navigation.navigate('article-content',{articleName:"condom"})
                break;
            case 'emer':
                props.navigation.navigate('article-content',{articleName:"emer"})
                break;
            case 'pill':
                props.navigation.navigate('article-content',{articleName:"pills"})
                break;
            case 'penis':
                props.navigation.navigate('article-content',{articleName:"penis"})
                break;
            case 'vergina':
                props.navigation.navigate('article-content',{articleName:"vergina"})
                break;
            case 'pin':
                props.navigation.navigate('article-content',{articleName:"pin"})
                break;
            case 'paper':
                props.navigation.navigate('article-content',{articleName:"paper"})
                break;
            case 'inject':
                props.navigation.navigate('article-content',{articleName:"inject"})
                break;
        }
        console.log('GO TO ARTICLE', menu);
    }


    const MENU_1 = (
        <View style={styles.card}>
            <TouchableOpacity style={styles.menu} onPress={()=>onPress('condom')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-condom.png')} />
                <Text style={styles.menuText}>ถุงยาง</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu} onPress={()=>onPress('inject')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-nuddle.png')} />
                <Text style={[styles.menuText]}>ฉีด</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu} onPress={()=>onPress('emer')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-emer-pill.png')} />
                <Text style={[styles.menuText]}>ยาคุมฉุกเฉิน</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu} onPress={()=>onPress('pill')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-pill.png')} />
                <Text style={[styles.menuText]}>ยาคุม</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menu} onPress={()=>onPress('pin')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-pin.png')} />
                <Text style={[styles.menuText]}>ฝังยาคุม</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu} onPress={()=>onPress('paper')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-paper.png')} />
                <Text style={[styles.menuText]}>แผ่นแปะคุมกำเนิด</Text>
            </TouchableOpacity>
        </View>
    )

    const MENU_2 = (
        <View style={styles.card}>
            <TouchableOpacity style={styles.menu} onPress={()=>onPress('penis')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-penis.png')} />
                <Text style={styles.menuText}>ทำหมันชาย</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu} onPress={()=>onPress('vergina')}>
                <Image
                    style={styles.menuImg}
                    source={require('../../assets/img/icon-vergina.png')} />
                <Text style={[styles.menuText]}>ทำหมันหญิง</Text>
            </TouchableOpacity>
        </View>
    )
    return (
        <SafeAreaLayout
            style={[globalStyle.safeArea]}
            insets='top'
        >
            <TopNavigation
                title={ type === "menu1" ? "การคุมกำเนิดชั่วคราว" : "การคุมกำเนิดถาวร"}
                alignment='center'
                leftControl={BackAction({ onPress:()=>props.navigation.goBack() })}
                titleStyle={globalStyle.headerTitleText}
                style={[globalStyle.headerTitle]}
            />
            {/*onPress={()=>props.navigation.goBack()}*/}

            <View style={[styles.layout]}>
                {type === "menu1" ? MENU_1 : MENU_2}
            </View>
        </SafeAreaLayout>
    )
}


const styles = StyleSheet.create({
    layout: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // flexWrap:'wrap',
        paddingHorizontal:5,
    },
    card:{
        backgroundColor:'#FFF',
        paddingVertical:20,
        paddingHorizontal:5,
        width:wp('95'),
        marginVertical:20,
        borderRadius:5,
        display:"flex",
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexWrap:'wrap',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:3,
        position:'relative',
    },
    menu:{
        backgroundColor:COLORS.HARD_RED_2,
        paddingVertical:7,
        paddingHorizontal:0,
        width:wp('42'),
        marginVertical:7,
        borderRadius:5,
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:8,
        position:'relative',
    },
    menuText:{
        fontSize:22,
        color:"#FFF",
        marginTop: 5
    },
    menuImg:{
        width: 64, height: 64
    },

});
