import React from 'react';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {Image, StyleSheet, View} from 'react-native';
import {Input, Text} from '@kitten/ui';
import {Button} from '../../components/button-component';
import {COLORS} from '../../styles/colors';
import {globalStyle} from '../../styles';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {CheckBox, Layout, Modal} from '@kitten/index';
import {AsyncStorage} from 'react-native';


export const SubMenu1Screen = (props): React.ReactElement => {
    const [visible, setVisible] = React.useState(false);
    const [checked, setChecked] = React.useState(false);
    const toggleModal = () => {
        setVisible(!visible);
    };

    React.useEffect(()=>{

    },[])

    return (
        <SafeAreaLayout
            style={globalStyle.safeArea}
            insets='top'
        >
            <View style={styles.layout}>
                {/*<Image*/}
                {/*    style={{width: 250, height: 250}}*/}
                {/*    source={require('../../assets/img/logo.png')} />*/}

                <View style={styles.card}>
                    <Text category={'h1'} style={styles.header}>วิธีการคุมกำเนิด มี 2 ประเภท</Text>

                    <Button style={styles.btnBar} onPress={()=>props.navigation.navigate('article-menu',{type:"menu1"})}> การคุมกำเนิดชั่วคราว  </Button>
                    <Button style={styles.btnBar} onPress={()=>props.navigation.navigate('article-menu',{type:"menu2"})}> การคุมกำเนิดถาวร </Button>
                    <Button outline style={[styles.btnBar,{marginTop:50}]} onPress={()=>props.navigation.goBack()}> กลับ </Button>

                </View>
            </View>

        </SafeAreaLayout>
    )
}


const styles = StyleSheet.create({
    layout: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    header:{
        // fontSize: 30,
        color: COLORS.HARD_RED,
        // fontWeight:'1000'
    },
    card:{
        backgroundColor:"#FFF",
        paddingVertical:20,
        paddingHorizontal:30,
        width:wp('85'),
        borderRadius:5,
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:3,
        position:'relative',
    },
    btnBar:{
        alignSelf: 'stretch',
    }
});
