import React, {useEffect, useRef, useState} from 'react';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {globalStyle} from '../../styles';
import {TopNavigation} from '@kitten/ui';
import {Text} from '@ui-kitten/components';
import {StyleSheet, View, TouchableOpacity, UIManager, LayoutAnimation, Image, ScrollView} from 'react-native';
import FoldView from 'react-native-foldview';
import Accordion from 'react-native-collapsible/Accordion';
import {_handleRender, _renderContent, _renderHeader} from '../../components/article-box.component';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {ARTICLE_CONTENT} from './data';
import {Video} from 'expo-av';
import VideoPlayer from 'expo-video-player'

export const ArticleContent = (props): React.ReactElement => {

    const [active, setActive] = useState([]);
    const [DATA,setDATA] = React.useState( ARTICLE_CONTENT.condom)
    useEffect(() => {
        if (props.route && props.route.params && props.route.params.articleName) {
            setDATA(ARTICLE_CONTENT[props.route.params.articleName] || ARTICLE_CONTENT.condom);
        }
    }, []);
    return (
        <SafeAreaLayout
            style={[globalStyle.safeArea]}
            insets='top'
        >
            <TopNavigation
                title={"การคุมกำเนิด"}
                alignment='center'
                titleStyle={globalStyle.headerTitleText}
                style={[globalStyle.headerTitle]}
            />
            <ScrollView>
                <View style={[styles.layout]}>
                    <View style={[styles.introduce,styles.card]}>
                        <View style={styles.introTitle}>
                            <Image
                                style={styles.introImg}
                                source={DATA.icon} />
                            <Text style={styles.introText} category={'h1'}>{DATA.title}</Text>
                        </View>

                        {DATA.short_desc_render  ?
                            DATA.short_desc_render()
                        :
                            <Text category={'h4'} style={{color:"#ffb6b9"}}>
                                {DATA.short_desc}
                            </Text>
                        }


                    </View>
                    {DATA.video && <View style={[styles.introduce,styles.card,{marginVertical: 10}]}>
                        <Video
                            source={DATA.video}
                            rate={1.0}
                            volume={1.0}
                            isMuted={false}
                            resizeMode={Video.RESIZE_MODE_CONTAIN}
                            useNativeControls
                            shouldPlay={false}
                            // isLooping
                            style={{ width: wp('70%'), height: wp('60%') }}
                        />
                        {/*<VideoPlayer*/}
                        {/*    videoProps={{*/}
                        {/*        shouldPlay: false,*/}
                        {/*        resizeMode: Video.RESIZE_MODE_CONTAIN,*/}
                        {/*        source: DATA.video,*/}
                        {/*        // useNativeControls: true*/}
                        {/*    }}*/}
                        {/*    width={wp('70%')}*/}
                        {/*    height={wp('60%')}*/}
                        {/*    inFullscreen={true}*/}
                        {/*    showFullscreenButton={false}*/}
                        {/*    videoBackground={"#FFF"}*/}
                        {/*/>*/}
                    </View>}


                    <Accordion
                        sections={DATA.content}
                        activeSections={active}
                        renderHeader={s=>_handleRender(s,'header',active)}
                        renderContent={s=>_handleRender(s,'body',active)}
                        onChange={(tab)=>setActive(tab)}
                        underlayColor={'rgba(255,255,255,0)'}
                        expandMultiple={true}
                        duration={900}
                    />
                </View>
            </ScrollView>
        </SafeAreaLayout>
    )
}

const styles = StyleSheet.create({
    layout:{
        flex:1,
        alignItems: 'center'
    },
    card:{
        backgroundColor:'#FFF',
        paddingVertical:20,
        paddingHorizontal:20,
        width:wp('90'),
        marginVertical:20,
        borderRadius:5,
        display:"flex",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap:'wrap',


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    introduce:{

    },
    introTitle:{
        display: 'flex',
        flexDirection: "row",
        alignItems: 'center',
        paddingVertical: 10
    },
    introImg:{
        width:40,
        height: 40,
    },
    introText:{
        color:"#ff8c85",
        paddingHorizontal: 10,
        fontSize: 50
    },
})
