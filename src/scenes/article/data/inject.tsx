import {Animated} from 'react-native';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {AtImg2, AtText} from '../../../components/article-render.component';

export const inject = {
    title:"ยาฉีดคุมกำเนิด",
    icon:require('../../../assets/img/icon-nuddle.png'),
    short_desc_render:()=>(
        <View style={{paddingBottom:20}}>
            <AtText text={"\t\t\t\t การฉีดยาคุมกำเนิดให้ผลในการป้องกันการตั้งครรภ์ได้ดีและเนื่องจากเป็นฮอร์โมนที่สังเคราะห์ขึ้นมาเหมือนฮอร์โมนเพศตามธรรมชาติจึงปลอดภัยสำหรับผู้ใช้"}/>
            <AtImg2 source={[
                require('../../../assets/img/contents/inject-1.jpeg'),
                require('../../../assets/img/contents/inject-2.jpeg'),
            ]}/>
        </View>
    ),
    video:require('../../../assets/video/inject.mov'),
    content:[
        {
            index:3,
            header:"ข้อดี",
            render:()=>(
                <View>
                    <AtText text={"1. มีประสิทธิภาพสูง "} mv={3}/>
                    <AtText text={"2. สามารถคุมกำเนิดได้นานถึง 12 สัปดาห์ "} mv={3}/>
                    <AtText text={"3. ไม่มีผลกับการมีเพศสัมพันธ์ "} mv={3}/>
                    <AtText text={"4. ใช้ได้ในทุกกลุ่มอายุ "} mv={3}/>
                    <AtText text={"5. สามารถใช้ได้ในแม่ที่ให้นมบุตร โดยคุณภาพน้ำนมไม่มีการเปลี่ยนแปลง "} mv={3}/>
                </View>
            )
        },
        {
            index:4,
            header:"ข้อจำกัด",
            render:()=>(
                <View>
                    <AtText text={"1. มีการเปลี่ยนแปลงของประจำเดือน"} mv={3}/>
                    <AtText text={"2. น้ำหนักตัวเพิ่มซึ่งวัยรุ่นอาจไม่ชอบ"} mv={3}/>
                    <AtText text={"3. หลังจากหยุดยาจะตั้งครรภ์ช้า"} mv={3}/>
                </View>
            )
        },
        {
            index:4,
            header:"ชนิดของยาฉีดคุมกำเนิดเป็นฮอร์โมนสังเคราะห์",
            headerSize:26,
            render:()=>(
                <View>
                    <AtText text={"ยาฉีดคุมกำเนิดชนิดที่มีฮอร์โมนโปรเจสตินโดยฉีดเข้ากล้ามเนื้อสะโพกทุก 12 สัปดาห์ เริ่มนัดฉีดภายใน 5 วันแรกของรอบประจำเดือน"} mv={3}/>
                </View>
            )
        },
        {
            index:4,
            header:"กลไกการออกฤทธิ์",
            render:()=>(
                <View>
                    <AtText text={"ยับยั้งการตกไข่  เยื่อบุโพรงมดลูกมีการเปลี่ยนแปลงไม่เหมาะสมกับการฝังตัวของไข่ที่ผสมแล้วและมูกที่ปากมดลูกเหนียวข้นมากขึ้นไม่เหมาะสำหรับการผ่านของอสุจิ"} mv={3}/>
                </View>
            )
        },
        {
            index:2,
            header:"ผลข้างเคียง",
            render:()=>(
                <View>
                    <AtText text={"อาจทำให้ไม่มีประจำเดือนหรือประจำเดือนมากะปริดกะปรอยในบางราย แต่ไม่ใช่อาการผิดปกติ หากมีประจำเดือนมามากเกินไปควรปรึกษาแพทย์"} />
                </View>
            )
        },
        {
            index:5,
            header:"ข้อห้ามใช้ ",
            render:()=>(
                <View>
                    <AtText text={"1. เป็นมะเร็งเต้านม"} mv={3}/>
                    <AtText text={"2. เลือดออกผิดปกติทางช่องคลอดที่ยังไม่ทราบสาเหตุ"} mv={3}/>
                    <AtText text={"3. ตั้งครรภ์ หรือสงสัยว่าตั้งครรภ์"} mv={3}/>
                </View>
            )
        },
        {
            index:5,
            header:"ข้อควรระวัง ",
            render:()=>(
                <View>
                    <AtText text={"1. เบาหวานที่ควบคุมน้ำตาลไม่ได้หรือมีโรคไต "} mv={3}/>
                    <AtText text={"2. เป็น/เคยเป็นโรคตับอักเสบหรือมะเร็งตับ"} mv={3}/>
                    <AtText text={"3. โรคที่เกี่ยวกับหลอดเลือดและหัวใจ"} mv={3}/>
                    <AtText text={"4. ความดันโลหิตสูงกว่า 160/100 มม.ปรอท"} mv={3}/>
                </View>
            )
        },
    ]
}
