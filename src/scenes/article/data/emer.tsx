import {Animated} from 'react-native';
import {Text} from '@kitten/ui';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {AtText, AtImg, AtImg2} from '../../../components/article-render.component';
import {COLORS} from '../../../styles/colors';

export const emer = {
    title:"ยาเม็ดคุมกำเนิดฉุกเฉิน ",
    icon:require('../../../assets/img/icon-emer-pill.png'),
    short_desc_render:()=>(<View>
        <AtText text={"\t\t\t เป็นยาที่รับประทานเพื่อป้องกันการตั้งครรภ์ไม่พึงประสงค์หลังจากมีเพศสัมพันธ์โดยไม่ได้ป้องกัน \n"}/>
    </View>),
    content:[
        {
            index:0,
            header:"ยาเม็ดคุมฉุกเฉินฮอร์โมนเดี่ยว",
            render:()=>(
                <View>
                    <AtText text={"\t\t\t\t ตัวยาสำคัญในยาคุมกำเนิดฉุกเฉินชนิดนี้คือ Levonorgestrel มี 2 ขนาด คือ เม็ดละ 0.75 mg ได้แก่ Postinor และ Madona รับประทาน 2 เม็ด ภายใน 3 วัน ( 72 ชั่วโมง) หลังมีเพศสัมพันธ์ที่ไม่ได้ป้องกัน บางยี่ห้ออาจเป็นชนิด เม็ดละ 1.5 mg  รับประทาน 1 เม็ด ภายใน 3 วัน (72 ชั่วโมง) "} mv={3}/>
                </View>
            )
        },
        {
            index:0,
            header:"ข้อดี",
            render:()=>(
                <View>
                    <AtText text={"1. ประสิทธิภาพการคุมกำเนิดสูง"} mv={3}/>
                    <AtText text={"2. วิธีการใช้ยาที่สะดวกและง่าย"} mv={3}/>
                    <AtText text={"3. เมื่อหยุดยาแล้ว สามารถกลับมามีบุตรได้ตามเดิม"} mv={3}/>
                </View>
            )
        },
        {
            index:2,
            header:"ข้อจำกัด",
            render:()=>(
                <View>
                    <AtText text={"1. ไม่สามารถป้องกันโรคติดต่อทางเพศสัมพันธ์ได้"} mv={3}/>
                    <AtText text={"2. มีประจำเดือนกระปริดกระปรอย"} mv={3}/>
                    <AtText text={"3. คลื่นไส้อาเจียน"} mv={3}/>
                    <AtText text={"4. มีประสิทธิภาพต่ำกว่ายาเม็ดคุมกำเนิด"} mv={3}/>
                </View>
            )
        },
    ]
}
