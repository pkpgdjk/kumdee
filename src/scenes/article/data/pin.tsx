import React from 'react';
import {StyleSheet, View} from 'react-native';
import {AtImg, AtImg2, AtText} from '../../../components/article-render.component';
import {COLORS} from '../../../styles/colors';
import {Text} from '@kitten/ui';

export const pin = {
    title:"ยาฝังคุมกำเนิด",
    icon:require('../../../assets/img/icon-pin.png'),
    short_desc_render:()=>(<View style={{paddingBottom:20}}>
        <Text style={{color:COLORS.ARTICLE_TEXT, fontSize:23}}>
            {'\t\t\t\t'}เป็นวิธีการคุมกำเนิดที่ได้รับการ <Text style={{color:COLORS.HARD_RED, fontSize:23}}> สนับสนุนจากรัฐบาลให้วัยรุ่นช่วงอายุ 10-20 ปี ใช้ โดยไม่เสียค่าใช้จ่าย</Text>เป็นวิธีคุมกำเนิดแบบชั่วคราวที่มีประสิทธิภาพสูง โดยใช้หลอดยาขนาดเล็ก มีลักษณะนิ่มยืดหยุ่นได้ ฝังใต้ท้องแขนท่อนบน ซึ่งภายในหลอดจะบรรจุฮอร์โมนโปรเจสติน ป้องกันการตั้งครรภ์ได้นาน 3 ปี (ชนิด 1 หลอด, Implanon®) หรือ 5 ปี (ชนิด 2 หลอด, Jadelle®) โดยเริ่มฝังยาฝังคุมกำเนิดภายใน 5 วันแรกของรอบประจำเดือนโดยฉีดยาชาก่อนทำการฝังทำให้ไม่รู้สึกเจ็บ และเมื่อครบกำหนดเวลาแล้วต้องถอดหลอดยาออก
        </Text>
        <AtImg2 source={[
            require('../../../assets/img/contents/pin-1.jpeg'),
            require('../../../assets/img/contents/pin-2.jpeg'),
        ]}/>
    </View>),
    video:require('../../../assets/video/pin.mp4'),
    content:[
        {
            index:0,
            header:"กลไกการออกฤทธิ์",
            render:()=>(
                <View>
                    <AtText text={"ยับยั้งการตกไข่  เยื่อบุโพรงมดลูกมีการเปลี่ยนแปลงไม่เหมาะสมกับการฝังตัวของไข่ที่ผสมแล้ว และมูกที่ปากมดลูกเหนียวข้นมากขึ้นไม่เหมาะสำหรับการผ่านของอสุจิ"} mv={3}/>
                </View>
            )
        },
        {
            index:1,
            header:"ข้อดี ",
            render:()=>(
                <View>
                    <AtText text={"1. มีประสิทธิภาพในการป้องกันการตั้งครรภ์สูงถึง 99% "} mv={3}/>
                    <AtText text={"2. คุมกำเนิดได้นาน "} mv={3}/>
                    <AtText text={"3. สามารถใช้ได้ในสตรีที่ให้นมบุตร โดยยาไม่มีผลต่อน้ำนม "} mv={3}/>
                    <AtText text={"4. ภายหลังถอดยาออกจะกลับสู่ภาวะเจริญพันธุ์ได้เร็วกว่ายาฉีดคุมกำเนิด"} mv={3}/>
                </View>
            )
        },
        {
            index:2,
            header:"ข้อจำกัด ",
            render:()=>(
                <View>
                    <AtText text={"1. มีการเปลี่ยนแปลงของประจำเดือน"} mv={3}/>
                    <AtText text={"2. อาจมีประจำเดือนมากะปริดกะปรอย"} mv={3}/>
                    <AtText text={"3. น้ำหนักตัวเพิ่มขึ้น"} mv={3}/>
                    <AtText text={"4. ปวดคัดตึงเต้านม"} mv={3}/>
                </View>
            )
        },
        {
            index:3,
            header:"ผลข้างเคียง ",
            render:()=>(
                <View>
                    <AtText text={"\t\t\t\t อาจมีเลือดออกกะปริบกะปรอยทางช่องคลอดในบางราย และดูแลแผลหลังจาก 7 วันแรกหลังการฝังยาฝังคุมกำเนิด ห้ามยกของหนักหรือออกกำลังกาย จากนั้นประจำเดือนมักจะมาห่าง ๆ หรือไม่มีประจำเดือนเลย ซึ่งการไม่มีประจำเดือนจากการใช้ยาฝังไม่ได้ส่งผลให้สุขภาพของสตรีแย่ลง ผลข้างเคียงอื่น ๆ เช่น การติดเชื้อบริเวณที่ฝังยา ปวดศีรษะ เป็นสิว น้ำหนักเพิ่มขึ้น คัดตึงเต้านม มีอารมณ์แปรปรวน แต่ผลข้างเคียงเหล่านี่พบได้น้อย"} mv={3}/>
                </View>
            )
        },
        {
            index:4,
            header:"ข้อห้ามใช้ ",
            render:()=>(
                <View>
                    <AtText text={"1. สตรีที่ตั้งครรภ์หรือสงสัยว่าตั้งครรภ์ "} mv={3}/>
                    <AtText text={"2. มีภาวะลิ่มเลือดอุดดันในหลอดเลือดดำระยะเฉียบพลัน  "} mv={3}/>
                    <AtText text={"3. เป็นมะเร็งหรือสงสัยว่าเป็นมะเร็งที่ตอบสนองต่อฮอร์โมนเพศ เช่น มะเร็งเต้านม   "} mv={3}/>
                    <AtText text={"4. มีหรือเคยมีเนื้องอกที่ตับ มีการทำงานของตับผิดปกติ "} mv={3}/>
                    <AtText text={"5. มีเลือดออกจากช่องคลอดที่ไม่ทราบสาเหตุ "} mv={3}/>
                </View>
            )
        },
        {
            index:5,
            header:"ข้อควรระวัง ",
            render:()=>(
                <View>
                    <AtText text={"1. เป็น/เคยเป็นโรคหัวใจและโรคหลอดเลือด "} mv={3}/>
                    <AtText text={"2. เป็น/เคยเป็นโรคตับอักเสบหรือมะเร็งตับ   "} mv={3}/>
                    <AtText text={"3. ความดันโลหิตสูงหรือเบาหวานที่ควบคุมไม่ได้"} mv={3}/>
                </View>
            )
        },
    ]
}
