import {condom} from './condom';
import {pills} from './pills';
import {inject} from './inject';
import {pin} from './pin';
import {paper} from './paper';
import {emer} from './emer';


export const ARTICLE_CONTENT = {
    condom,
    pills,
    inject,
    pin,
    paper,
    emer,
}
