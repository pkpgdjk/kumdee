import React, {useEffect, useRef, useState} from 'react';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {globalStyle} from '../../styles';
import {TopNavigation} from '@kitten/ui';
import {Text} from '@ui-kitten/components';
import {StyleSheet, View, TouchableOpacity, UIManager, LayoutAnimation, Image, ScrollView} from 'react-native';
import FoldView from 'react-native-foldview';
import Accordion from 'react-native-collapsible/Accordion';
import {_handleRender, _renderContent, _renderHeader} from '../../components/article-box.component';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {ARTICLE_CONTENT} from './data';
import {backgroundColor} from '@eva-design/eva/mapping';
import {Paragraph, ParagraphText,ParagraphMiniHeader} from '../../components/Paragraph.component';
import { ArticleHeader } from '../../components/ArticleHeader.component';

export const CondomContent = (props): React.ReactElement => {
    return (
        <SafeAreaLayout
            style={[globalStyle.safeArea,{backgroundColor:"#FFF"}]}
            insets='top'
        >
            <TopNavigation
                title={"การคุมกำเนิด ด้วยถุงยาง"}
                alignment='center'
                titleStyle={globalStyle.headerTitleText}
                style={[globalStyle.headerTitle]}
            />
            <ScrollView>
                <View style={[styles.layout]}>
                    <Paragraph width={'100%'} bgColor={'#2dc3ff'} color={"#fff"}>
                        <ParagraphText color={"#FFF"}>
                            \t หมายถึง อุปกรณ์ประเภทสิ่งกีดขวางที่ใช้ระหว่างร่วมเพศเพื่อลดโอกาสการตั้งครรภ์หรือการติดโรคติดต่อ ทางเพศสัมพันธ์
                        </ParagraphText>
                    </Paragraph>

                    {/*<ArticleHeader color={"#FFF"} bgColor={"#ff7f19"}></ArticleHeader>*/}
                    <Paragraph width={'100%'} bgColor={'#ff7f19'}>
                        <ParagraphMiniHeader width={200} bgColor={"#ffd14b"} color={"#FFF"}>วิธีการใส่ถุงยางอนามัย</ParagraphMiniHeader>
                        <ParagraphText color={"#FFF"}>
                            - อวัยวะเพศต้องอยู่ในสภาพแข็งตัวเต็มที่ก่อน {'\n'}
                            - ในขั้นตอนการแกะซองเพื่อใช้งาน ควรฉีกซองอย่างระมัดระวัง ไม่ควรใช้ฟันฉีก หรือใช้กรรไกรตัด เนื่องจากอาจโดนส่วนของถุงยางทำให้ถุงยางรั่วได้ {'\n'}
                            - เวลาใส่ให้รอยม้วนของขอบถุงยางอยู่ด้านนอก {'\n'}
                            - กรณีถุงยางอนามัยชนิดที่มีกระเปาะตรงปลาย (สำหรับเก็บน้ำอสุจิ) ให้บีบที่กระเปาะเพื่อไล่อากาศออกให้หมดในขณะใส่
                            แต่สำหรับชนิดที่ไม่มีกระเปาะ ให้บีบที่ปลายถุงยางประมาณครึ่งนิ้วเพื่อให้เกิดพื้นที่สำหรับเก็บน้ำอสุจิ {'\n'}
                            - ใช้อีกมือหนึ่งรูดถุงยางอนามัยให้แนบไปกับอวัยวะเพศ รูดลงมาจนถึงส่วนโคนอวัยวะเพศ {'\n'}
                        </ParagraphText>

                        <ParagraphMiniHeader width={200} bgColor={"#ffd14b"} color={"#FFF"}>วิธีการถอดถุงยางอนามัย</ParagraphMiniHeader>
                        <ParagraphText color={"#FFF"}>
                            - หลังจากที่มีการหลั่งน้ำอสุจิแล้ว ควรถอนอวัยวะเพศพร้อมถุงยางอนามัยออกจากช่องคลอดในขณะที่อวัยวะเพศยังแข็งตัวอยู่
                            โดยขณะถอนอวัยวะเพศควรบีบรัดถุงยางให้แนบกับโคนอวัยวะเพศ เพื่อป้องกันน้ำอสุจิไม่ให้หกหรือไหลย้อนกลับมา {'\n'}
                            - ใช้กระดาษทิชชูจับตรงขอบถุงยางแล้วค่อยๆ รูดออก โดยไม่ให้น้ำอสุจิในถุงหกเลอะเทอะ {'\n'}
                            - ตรวจสอบถุงยางด้วยตาว่าถุงยางไม่มีการแตกรั่ว {'\n'}
                            - จากนั้นทิ้งลงในถังขยะที่ปิดมิดชิด หากจะผูกมัดปากถุงยางเพื่อป้องกันน้ำอสุจิไหลออกมาเลอะเทอะในถังขยะ ควรทำด้วยความระมัดระวัง
                            หลีกเลี่ยงการสัมผัสสารคัดหลั่งที่ติดมากับถุงยางให้น้อยที่สุด เพื่อลดความเสี่ยงของโรคติดต่อทางเพศสัมพันธ์ {'\n'}

                        </ParagraphText>
                    </Paragraph>


                    <Paragraph width={'100%'} bgColor={'#00aa14'}>
                        <ParagraphMiniHeader width={200} bgColor={"#ff0062"} color={"#FFF"}>กลไกการออกฤทธิ์</ParagraphMiniHeader>
                        <ParagraphText color={"#FFF"}>
                            \t ถุงยางอนามัยใช้สวมใส่อวัยวะเพศชายเมื่อแข็งตัวเต็มที่ ทำหน้าที่เป็นเครื่องกีดขวางไม่ให้ตัวเชื้ออสุจิเข้าสู่ช่องคลอด ที่ปลายถุงยางอนามัยจะมีกระเปราะเล็กๆสำหรับรองรับน้ำอสุจิ
                        </ParagraphText>
                    </Paragraph>

                    <View style={{flex:1,flexDirection:"row"}}>
                        <Paragraph width={'50%'} bgColor={'#ff7f19'}>
                            <ParagraphMiniHeader width={150} bgColor={"#807cff"} color={"#FFF"}>กลไกการออกฤทธิ์</ParagraphMiniHeader>
                            <ParagraphText color={"#FFF"}>
                                1.คุมกำเนิด {"\n"}
                                2.ป้องกันโรคติดต่อทางเพศสัมพันธ์ เช่น หนองใน, ซิฟิลิส, เริม, ไวรัสตับอักเสบบี, ไวรัสเอชพีวี (HPV) และไวรัสเอชไอวี (HIV) เป็นต้น {"\n"}
                                3.ลดการบาดเจ็บจากการมีเพศสัมพันธ์ {"\n"}
                                4.ช่วยเพิ่มอรรถรสทางเพศได้ {"\n"}
                            </ParagraphText>
                        </Paragraph>

                        <Paragraph width={'50%'} bgColor={'#ff0062'}>
                            <ParagraphMiniHeader width={150} bgColor={"#00910f"} color={"#FFF"}>กลไกการออกฤทธิ์</ParagraphMiniHeader>
                            <ParagraphText color={"#FFF"}>
                                1.การระคายเคืองเฉพาะที่ พบได้ถ้าการหล่อลื่นไม่เพียงพอ \n
                                2.การแพ้ถุงยางหรือแพ้สารเคมีที่เคลือบถุงยาง \n
                            </ParagraphText>

                            <ParagraphMiniHeader width={150} bgColor={"#00910f"} color={"#FFF"}>ข้อห้าม </ParagraphMiniHeader>
                            <ParagraphText color={"#FFF"}>
                                ห้ามใช้สำหรับผู้ที่แพ้กาวลาเท็กซ์
                            </ParagraphText>
                        </Paragraph>
                    </View>

                    <Paragraph width={'100%'} bgColor={'#ff7cb4'}>
                        <ParagraphMiniHeader width={200} bgColor={"#ff0062"} color={"#FFF"}>ข้อควรระวัง</ParagraphMiniHeader>
                        <ParagraphText color={"#FFF"}>
                            1.ขนาดของถุงยางอนามัย  ให้วัดรอบวงของอวัยวะเพศขณะแข็งตัวเต็มที่เป็นหน่วยมิลลิเมตร และนำไปหารด้วย 2 จะได้เป็นขนาดของถุงยางอนามัยที่เหมาะสม {"\n"}
                            2.ห้ามใช้น้ำมันหรือโลชั่นเป็นสารหล่อลื่นจะทำให้ถุงยางอนามัยเกิดการฉีกขาดได้ง่ายในขณะมีเพศสัมพันธ์ ดังนั้นควรใช้สารหล่อลื่นที่มีส่วนผสมของซิลิโคนเท่านั้น {"\n"}
                            3.ถุงยางอนามัยมีวันหมดอายุ  ควรตรวจสอบวันหมดอายุก่อนนำมาใช้งานทุกครั้ง เนื่องจากสารหล่อลื่นที่อยู่ในซองถุงยางอนามัยอาจเสื่อมสภาพ {"\n"}
                            4.บีบไล่อากาศที่ปลายถุงยางก่อนใส่ทุกครั้ง เพราะอาจจะทำให้ถุงยางอนามัยแตกหรือฉีกขาดได้ง่าย {"\n"}
                            5.สวมถุงยางอนามัยให้ถูกด้าน หันด้านที่มีกระเปาะตรงส่วนหัวออกด้านนอก และสวมลงบนอวัยวะเพศที่แข็งตัวอยู่ {"\n"}
                        </ParagraphText>
                    </Paragraph>

                </View>
            </ScrollView>
        </SafeAreaLayout>
    )
}

const styles = StyleSheet.create({
    layout:{
        flex:1,
        alignItems: 'center'
    },
    card:{
        backgroundColor:'#FFF',
        paddingVertical:20,
        paddingHorizontal:20,
        width:wp('90'),
        marginVertical:20,
        borderRadius:5,
        display:"flex",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap:'wrap',


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
})
