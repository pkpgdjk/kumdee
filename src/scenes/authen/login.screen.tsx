import React, {useEffect, useState} from 'react';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {StyleSheet, View, Image, AsyncStorage, Alert} from 'react-native';
import {Divider, TopNavigation} from '@kitten/ui';
import {
    // Button,
    Input,
    Layout,
    Text,
    useTheme,
} from '@ui-kitten/components';
import {COLORS} from '../../styles/colors';
import {globalStyle} from '../../styles';
import {utils} from '../../utils';
import { human } from 'react-native-typography'
import {colors} from 'react-native-svg/lib/typescript/lib/extract/extractColor';
import {Button} from '../../components/button-component';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {LoadingScreen} from '../loading.screen';
import axios from "axios"
import {CONSTANTS} from '../../constants';
import {err} from 'react-native-svg/lib/typescript/xml';


export const LoginScreen = (props): React.ReactElement => {
    const username = utils.useInputChanges('');
    const password = utils.useInputChanges('');
    const [loading, setLoading] = useState(true);

    const onLoad = async () =>{
        // await AsyncStorage.removeItem('token');
        try {
            const token = await AsyncStorage.getItem('token');
            if (token !== null) {
                console.log('GET TOKEN', token);
                axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
                let profile = await axios.get(CONSTANTS.URL + '/profile')
                props.navigation.navigate('main')
            }
        } catch (error) {
            console.log("on load",error)
            return;
        }
        setLoading(false)
    }

    useEffect(()=>{
        onLoad();
    },[])

    const pressLoading = async () =>{
        if (!username.value || !username.value){
            Alert.alert(
                'บางสิ่งผิดพลาด',
                'กรุณากรอกข้อมูลการล็อคอินให้ถูกต้อง',
                [
                    {text: 'OK'},
                ],
                { cancelable: true }
            )
            return;
        }
        setLoading(true);
        try {
            let login = await axios.post(CONSTANTS.URL + '/login',{
                username:username.value,
                password:password.value
            })
            console.log("SET TOKEN",login.data.data.token)
            AsyncStorage.setItem('token', login.data.data.token);
            axios.defaults.headers.common['Authorization'] = `Bearer ${login.data.data.token}`;
            setTimeout(()=>props.navigation.navigate('main'),1800)
        }catch (e) {
            console.log(e);
            Alert.alert(
                'บางสิ่งผิดพลาด',
                'การล็อคอินล้มเหลว',
                [
                    {text: 'OK'},
                ],
                { cancelable: true }
            )
        }
        setLoading(false);
    }



    if (loading){
        return <LoadingScreen/>;
    }
    return (
        <SafeAreaLayout
            style={globalStyle.safeArea}
            insets='top'
            level='2'>

            {/*<TopNavigation*/}
            {/*    title='เข้าสู่ระบบ'*/}
            {/*    alignment='center'*/}
            {/*    titleStyle={globalStyle.headerTitleText}*/}
            {/*    style={globalStyle.headerTitle}*/}
            {/*/>*/}

            <View style={styles.layout}>
                <Image
                    style={{width: 300, height: 300}}
                    source={require('../../assets/img/logo.png')} />

                <View style={styles.loginPanel}>
                    <Text category={'h1'} style={styles.header}>เข้าสู่ระบบ</Text>
                    <Input
                        style={styles.input}
                        textStyle={styles.inputText}
                        size='large'
                        placeholder='กรอกชื่อผู้ใช้'
                        {...username}
                    />

                    <Input
                        style={styles.input}
                        textStyle={styles.inputText}
                        size='large'
                        placeholder='กรอกรหัสผ่าน'
                        secureTextEntry={true}
                        {...password}
                    />
                </View>
               <View style={{display:'flex',justifyContent:'center',alignItems:'center',flexDirection:'row',paddingHorizontal:30}}>
                   <Button style={{marginRight:5,flex:1,paddingHorizontal:5}} onPress={()=>props.navigation.navigate('register')}>
                       สมัครสมาชิก
                   </Button>

                   <Button style={{marginLeft:5,flex:1,paddingHorizontal:5}} onPress={pressLoading}>
                       เข้าสู่ระบบ
                   </Button>
               </View>
            </View>
        </SafeAreaLayout>
    );
}


const styles = StyleSheet.create({
    layout: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    header:{
        // fontSize: 30,
        color: COLORS.HARD_RED,
        // fontWeight:'1000'
    },
    input: {
        marginVertical:8,
        width:'100%',
        borderColor: COLORS.RED_2,
        backgroundColor: COLORS.RED_1,
        color:"#FFF"
    },
    button:{
        backgroundColor:COLORS.HARD_RED,
        borderColor:COLORS.HARD_RED,
        marginVertical:20,
        paddingHorizontal: 30,
        fontFamily:'opensans-regular',
        paddingVertical:15,

    },
    buttonText:{
        fontSize:18,
        fontFamily:'opensans-regular',
    },
    loginPanel:{
        backgroundColor:"#FFF",
        paddingVertical:20,
        paddingHorizontal:30,
        width:wp('85'),
        borderRadius:5,
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:3,
        position:'relative',
    },
    inputText:{
        fontSize: 25
    }
});
