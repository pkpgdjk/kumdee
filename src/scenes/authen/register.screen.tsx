import React,{useState} from 'react';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {StyleSheet, View, Image, Alert, AsyncStorage, ScrollView} from 'react-native';
import {Divider, TopNavigation} from '@kitten/ui';
import {
    // Button,
    Input,
    Layout, Radio, RadioGroup,
    Text,
    useTheme,
} from '@ui-kitten/components';
import {COLORS} from '../../styles/colors';
import {globalStyle} from '../../styles';
import {utils} from '../../utils';
import { human } from 'react-native-typography'
import {colors} from 'react-native-svg/lib/typescript/lib/extract/extractColor';
import {Button} from '../../components/button-component';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {LoadingScreen} from '../loading.screen';
import axios from 'axios';
import {CONSTANTS} from '../../constants';
import {KeyboardAvoidingView} from 'react-native';


export const RegisterScreen = (props): React.ReactElement => {
    const username = utils.useInputChanges('');
    const password = utils.useInputChanges('');
    const email = utils.useInputChanges('');
    const confirmPassword = utils.useInputChanges('');
    const age = utils.useInputChanges('');
    const hasSexBefore = utils.useRadioChanges(null);
    const hasCouple = utils.useRadioChanges(null);


    const [loading, setLoading] = useState(false);

    const pressLoading = async () =>{
        if (!username.value ||
            !username.value ||
            !email.value||
            !confirmPassword.value||
            !age.value||
            hasSexBefore.selectedIndex == null ||
            hasCouple.selectedIndex == null
    ){
            Alert.alert(
                'บางสิ่งผิดพลาด',
                'กรุณากรอกข้อมูลให้ถูกต้อง',
                [
                    {text: 'OK'},
                ],
                { cancelable: true }
            )
            return;
        }
        setLoading(true);
        console.log("REGISTER",{
            username:username.value,
            password:password.value,
            email:email.value,
            confirmPassword:confirmPassword.value,
            age:age.value,
            hasSexBefore:hasSexBefore.selectedIndex,
            hasCouple:hasCouple.selectedIndex,
        })
        try {
            let register = await axios.post(CONSTANTS.URL + '/register',{
                username:username.value,
                password:password.value,
                email:email.value,
                confirmPassword:confirmPassword.value,
                age:age.value,
                hasSex:parseInt(hasSexBefore.selectedIndex) == 0 ? "true" : "false",
                hasCouple:parseInt(hasCouple.selectedIndex) == 0 ? "true" : "false",
            })
            Alert.alert(
                'สมัครสมาชิกสำเร็จ',
                'คุณสามารถล็อคอินเข้าสู้ระบบได้แล้ว',
                [
                    {text: 'OK'},
                ],
                { cancelable: true }
            )
            setTimeout(()=>props.navigation.navigate('login'),1800)
        }catch (e) {
            console.log(e.response.data);
            let message = "การสมัครสมาชิกเกิดข้อผิดพลาด"
            if (e.response && e.response.data && e.response.data.errors){
               message = e.response.data.errors[0]['msg']
            }
            Alert.alert(
                'บางสิ่งผิดพลาด',
                message,
                [
                    {text: 'OK'},
                ],
                { cancelable: true }
            )

        }
        setLoading(false);
    }



    if (loading){
        return <LoadingScreen/>;
    }
    return (
        <SafeAreaLayout
            style={globalStyle.safeArea}
            insets='top'
            level='2'>

            {/*<TopNavigation*/}
            {/*    title='เข้าสู่ระบบ'*/}
            {/*    alignment='center'*/}
            {/*    titleStyle={globalStyle.headerTitleText}*/}
            {/*    style={globalStyle.headerTitle}*/}
            {/*/>*/}

            <KeyboardAvoidingView  enabled>
                <ScrollView >
                    <View  style={styles.layout}>

                {/*<Image*/}
                {/*    style={{width: 300, height: 300}}*/}
                {/*    source={require('../../assets/img/logo.png')} />*/}

                <View style={styles.loginPanel}>
                    <Text category={'h1'} style={styles.header}>ลงทะเบียนเข้าใช้งาน</Text>
                    <Input
                        style={styles.input}
                        textStyle={styles.inputText}
                        size='large'
                        placeholder='กรอกชื่อผู้ใช้'
                        {...username}
                    />

                    <Input
                        style={styles.input}
                        textStyle={styles.inputText}
                        size='large'
                        placeholder='กรอกอีเมล'
                        {...email}
                    />

                    <Input
                        style={styles.input}
                        textStyle={styles.inputText}
                        size='large'
                        placeholder='กรอกรหัสผ่าน'
                        secureTextEntry={true}
                        {...password}
                    />

                    <Input
                        style={styles.input}
                        textStyle={styles.inputText}
                        size='large'
                        placeholder='กรอกรหัสผ่านอีกครั้ง'
                        secureTextEntry={true}
                        {...confirmPassword}
                    />

                    <Input
                        style={styles.input}
                        textStyle={styles.inputText}
                        size='large'
                        placeholder='กรอกอายุ'
                        keyboardType='numeric'
                        {...age}
                    />

                    <View style={{flexDirection:"row",padding:15}}>
                        <View>
                            <Text style={{fontSize:20}}>เคยมีเพศสัมพันธ์หรือยัง?</Text>
                            <RadioGroup
                                {...hasSexBefore} >
                                <Radio style={{marginVertical: 3}} textStyle={{fontSize:20}} text='เคยมีเพศสัมพันธ์แล้ว'/>
                                <Radio style={{marginVertical: 3,}} textStyle={{fontSize:20}} text='ยังไม่เคยมีเพศสัมพันธ์'/>
                            </RadioGroup>
                        </View>

                        <View>
                            <Text style={{fontSize:20}}>เคยมีแฟนมาก่อนหรือปล่าว?</Text>
                            <RadioGroup
                                {...hasCouple} >
                                <Radio style={{marginVertical: 3,}} textStyle={{fontSize:20}} text='มีแฟนแล้ว'/>
                                <Radio style={{marginVertical: 3,}} textStyle={{fontSize:20}} text='ยังไม่มีแฟน'/>
                            </RadioGroup>
                        </View>
                    </View>


                </View>
               <View style={{display:'flex',justifyContent:'center',alignItems:'center',flexDirection:'row',paddingHorizontal:30}}>
                   <Button style={{marginRight:5,flex:1,paddingHorizontal:5}} outline onPress={()=>props.navigation.navigate('login')}>
                       ยกเลิก
                   </Button>
                   <Button style={{marginLeft:5,flex:2,paddingHorizontal:5}} onPress={pressLoading}>
                       ลงทะเบียน
                   </Button>
               </View>
                    </View>

                </ScrollView>

            </KeyboardAvoidingView>
        </SafeAreaLayout>
    );
}


const styles = StyleSheet.create({
    layout: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    header:{
        // fontSize: 30,
        color: COLORS.HARD_RED,
        // fontWeight:'1000'
    },
    input: {
        marginVertical:8,
        width:'100%',
        borderColor: COLORS.RED_2,
        backgroundColor: COLORS.RED_1,
        color:"#FFF"
    },
    button:{
        backgroundColor:COLORS.HARD_RED,
        borderColor:COLORS.HARD_RED,
        marginVertical:20,
        paddingHorizontal: 30,
        fontFamily:'opensans-regular',
        paddingVertical:15,

    },
    buttonText:{
        fontSize:18,
        fontFamily:'opensans-regular',
    },
    loginPanel:{
        backgroundColor:"#FFF",
        paddingVertical:20,
        paddingHorizontal:30,
        width:wp('85'),
        borderRadius:5,
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:3,
        position:'relative',
    },
    inputText:{
        fontSize: 25
    }
});
