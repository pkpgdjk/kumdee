import React,{useRef,useEffect} from 'react';
import {SafeAreaLayout} from '../components/safe-area-layout.component';
import {StyleSheet, View,Image} from 'react-native';
import {
    Button,
    Input,
    Layout,
    Text,
    useTheme,
} from '@ui-kitten/components';
import LottieView from "lottie-react-native";


export const LoadingScreen = (props): React.ReactElement => {
    const refLoading1 = useRef();
    const refLoading2 = useRef();

    useEffect(()=>{
        refLoading1.current.play();
        refLoading2.current.play();
    },[])

    return (
        <SafeAreaLayout style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <LottieView
                ref={refLoading2}
                style={{
                    width: 250,
                    height: 250,
                    // backgroundColor: '#eee',
                }}
                source={require('../assets/lottie/4452-dr-consultation.json')}
            />
            <LottieView
                ref={refLoading1}
                style={{
                    width: 100,
                    height: 100,
                    // backgroundColor: '#eee',
                }}
                source={require('../assets/lottie/196-material-wave-loading.json')}
            />
        </SafeAreaLayout>
    )
}
