import React,{useState,useRef,useEffect} from 'react';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {Image, StyleSheet, View} from 'react-native';
import {Input, Text} from '@kitten/ui';
import {Button} from '../../components/button-component';
import {COLORS} from '../../styles/colors';
import {globalStyle} from '../../styles';
import {TopNavigation} from '@ui-kitten/components';
import preTest from './pre.json';
import postTest from './pre.json';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LottieView from "lottie-react-native";
import {Audio} from 'expo-av';


export const SurveyResultScreen = (props): React.ReactElement => {
    const TEST = preTest;
    let [ans,setAns] = useState(new Array(TEST.length));
    const [score,setScore] = React.useState( 0)
    const [type,setType] = React.useState( null)
    const [message,setMessage] = React.useState( "")
    const [image,setImage] = React.useState( null)


    const firework = useRef();

    const playSound = async (source)=>{
        if (!source) return;
        const soundObject = new Audio.Sound();
        try {

            await soundObject.loadAsync(source);
            await soundObject.playAsync();
            // Your sound is playing!
        } catch (error) {
            // An error occurred!
        }
    }


    useEffect(()=>{


        if(firework.current) firework.current.play();
        if (props.route && props.route.params && props.route.params.type) {
            setType(props.route.params.type);
        }

        if (props.route && props.route.params && props.route.params.score) {
            setScore(props.route.params.score);
            if (props.route.params.score >=17){
                setMessage('ดีมาก');
                setImage(require('../../assets/img/very-good.png'))
                setTimeout(()=>playSound(require('../../assets/sound/very-good.aac')),1000)
            }else if (props.route.params.score >= 13){
                setMessage('ดี');
                setImage(require('../../assets/img/good.png'))
                setTimeout(()=>playSound(require('../../assets/sound/good.aac')),1000)
            }else if (props.route.params.score >= 9){
                setMessage('ปานกลาง');
                setImage(require('../../assets/img/meduim.png'))
                setTimeout(()=>playSound(require('../../assets/sound/meduim.aac')),1000)
            }else if (props.route.params.score >=5){
                setMessage('พอใช้');
                setImage(require('../../assets/img/low.png'))
                setTimeout(()=>playSound(require('../../assets/sound/low.aac')),1000)
            }else{
                setMessage('ปรับปรุง');
                setImage(require('../../assets/img/very-low.png'))
                setTimeout(()=>playSound(require('../../assets/sound/very-low.aac')),1000)
            }
        }

    },[])

    const pressBack = () =>{
        props.navigation.navigate('main')
    }

    return (
        <SafeAreaLayout
            style={[globalStyle.safeArea]}
            insets='top'
        >
            <TopNavigation
                title={`คะแนนแบบสอบถาม ` + (props.type ==="pre" ? "ก่อนการใช้งาน" : "หลังการใช้งาน")}
                alignment='center'
                titleStyle={globalStyle.headerTitleText}
                style={[globalStyle.headerTitle]}
            />

            <View style={styles.layout}>
                <View style={styles.card}>
                    <Text category={'h1'} style={{color:COLORS.RED}}>คะแนนของคุณคือ</Text>
                    <Text category={'h1'} style={{fontSize:60,marginVertical: 10,color:COLORS.RED}}>{score}/{TEST.length}</Text>

                    {image && <Image
                        style={{width: 250, height: 250,marginVertical:15}}
                        source={image} /> }

                    <Text category={'h1'} style={{fontSize:50,marginVertical: 10,color:COLORS.RED}}>{message}</Text>




                    <LottieView
                        ref={firework}
                        style={{
                            width: 300,
                            height: 300,
                            position: 'absolute',
                            top:-20,
                        }}
                        source={require('../../assets/lottie/4479-fireworks.json')}
                    />


                </View>

                <View style={styles.btnActionWrapper}>
                    <Button style={styles.btnNext} color={COLORS.HARD_RED} onPress={pressBack} > กลับหน้าแรก </Button>
                </View>
            </View>
        </SafeAreaLayout>
    )
}


const styles = StyleSheet.create({
    layout: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    card:{
        backgroundColor:"#FFF",
        paddingVertical:20,
        paddingHorizontal:30,
        width:wp('90'),
        marginVertical:20,
        borderRadius:5,
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:1,
        position:'relative',
    },

    btnActionWrapper:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        width:wp('90'),
        marginTop:'auto'
    },
    btnNext:{
        flex:1,
        marginHorizontal:5,
        paddingHorizontal:10,
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0,

        // its for android
        elevation:2,
        position:'relative',
    },
});
