import React, {useEffect, useState} from 'react';
import {SafeAreaLayout} from '../../components/safe-area-layout.component';
import {Alert, AsyncStorage, Image, StyleSheet, View} from 'react-native';
import {Input, Text} from '@kitten/ui';
import {Button} from '../../components/button-component';
import {COLORS} from '../../styles/colors';
import {globalStyle} from '../../styles';
import {TopNavigation} from '@ui-kitten/components';
import preTest from './pre.json';
import postTest from './pre.json';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import axios from 'axios';
import {CONSTANTS} from '../../constants';


export const SurveyScreen = (props): React.ReactElement => {
    const [testId, setTestId] = useState(0);
    const [select, setSelect] = useState(0);
    const [type,setType] = React.useState( null)

    const TEST = preTest;

    let [ans,setAns] = useState(new Array(TEST.length));

    useEffect(() => {
        if (props.route && props.route.params && props.route.params.type) {
            setType(props.route.params.type);
        }
    }, []);


    const pressBack = () =>{
        if (testId-1 >= -1){
            console.log("BACK")
            setSelect(ans[testId - 1]);
            setTestId(testId - 1);
        }
    }

    const pressNext = async () =>{
        if (testId+1 < TEST.length){
            console.log("NEXT",select)
            ans[testId] = select;
            setAns(ans);

            setSelect(ans[testId+1])
            setTestId(testId+1)
        }else {
            ans[testId] = select;
            setAns(ans);

            let count = 0;
            TEST.map((t, i) => {
                if (t.correct == ans[i]) {
                    count++;
                }
            });

            try {
                let profile  = await AsyncStorage.getItem('profile');
                profile = JSON.parse(profile);
                let savetest = await axios.post(CONSTANTS.URL + '/' + (type === 'pre' ? 'savepre' : 'savepost'), {
                    data: {
                        score:count,
                        username: profile.username || '',
                        email: profile.email || '',
                        age: profile.age || '',
                        hasSex: profile.hasSex || '',
                        hasCouple: profile.hasCouple || '',
                    }
                });
            }catch (e) {
                console.log(e.response);
                Alert.alert(
                    'บางสิ่งผิดพลาด',
                    'การบันทึกข้อมูลล้มเหลว',
                    [
                        {text: 'OK'},
                    ],
                    { cancelable: true }
                )
            }

            props.navigation.navigate('survey-result', {score: count,type});
            console.log('FINISH', ans);
        }

    }

    const pressSelect = (id) =>{
        setSelect(id)
    }

    console.log("NOW",testId,select,ans)
    return (
        <SafeAreaLayout
            style={[globalStyle.safeArea]}
            insets='top'
        >
            <TopNavigation
                title={`แบบสอบถาม ` + (type ==="pre" ? "ก่อนการใช้งาน" : "หลังการใช้งาน")}
                alignment='center'
                titleStyle={globalStyle.headerTitleText}
                style={[globalStyle.headerTitle]}
            />

            <View style={styles.layout}>
                <View style={styles.card}>
                    <Text category={'h4'} style={styles.question}>{TEST[testId].question}</Text>


                    {/*<View style={styles.ansGroup}>*/}
                        <Button style={styles.btnBar}
                                outline={select!==1}
                                onPress={()=>pressSelect(1)}
                                color={COLORS.RED_3}
                                textStyle={{textAlign:'left',fontSize:22}}>
                            {TEST[testId].ans_1}
                        </Button>
                        <Button style={styles.btnBar}
                                outline={select!==2}
                                onPress={()=>pressSelect(2)}
                                color={COLORS.RED_3}
                                textStyle={{textAlign:'left',fontSize:22}}>
                            {TEST[testId].ans_2}
                        </Button>
                        <Button style={styles.btnBar}
                                outline={select!==3}
                                onPress={()=>pressSelect(3)}
                                color={COLORS.RED_3}
                                textStyle={{textAlign:'left',fontSize:22}}>
                            {TEST[testId].ans_3}
                        </Button>
                        <Button style={styles.btnBar}
                                outline={select!==4}
                                onPress={()=>pressSelect(4)}
                                color={COLORS.RED_3}
                                textStyle={{textAlign:'left',fontSize:22}}>
                            {TEST[testId].ans_4}
                        </Button>
                    {/*</View>*/}
                </View>

                <View style={styles.btnActionWrapper}>
                    <Button style={styles.btnBack} color={COLORS.HARD_RED} onPress={pressBack} disabled={testId==0}> ย้อนกลับ </Button>
                    <Button style={styles.btnNext} color={COLORS.HARD_RED} onPress={pressNext} disabled={select==0}>  { testId+1 === TEST.length ? "ดูผลลัพธ์" : `ต่อไป (${testId+1}/${TEST.length})`} </Button>

                </View>
            </View>
        </SafeAreaLayout>
    )
}


const styles = StyleSheet.create({
    layout: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    card:{
        backgroundColor:"#FFF",
        paddingVertical:20,
        paddingHorizontal:30,
        width:wp('90'),
        marginVertical:20,
        borderRadius:5,
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

        // its for android
        elevation:1,
        position:'relative',
    },
    ansGroup:{
        display:'flex',
        justifyContent:"center",
        alignItems:'center',
        flexDirection: "row",
        flexWrap: 'wrap',
        // alignItems: 'flex-start'
    },
    btnBar:{
        alignSelf: 'stretch',
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 8,
        // backgroundColor:
    },
    question:{
        // fontSize: 30,
        color: COLORS.RED,
        marginBottom:15,
        // fontWeight:'1000'
    },

    btnActionWrapper:{
      display:'flex',
      flexDirection:'row',
      justifyContent:'center',
        width:wp('90'),
        marginTop:'auto'
    },
    btnNext:{
        flex:1,
        marginHorizontal:5,
        paddingHorizontal:10,
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0,

        // its for android
        elevation:2,
        position:'relative',
    },
    btnBack:{
        flex:1,
        marginHorizontal:5,
        paddingHorizontal:10,
        //Its for IOS
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0,

        // its for android
        elevation:2,
        position:'relative',
    }
});
